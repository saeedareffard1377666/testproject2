import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { pipe } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}
  Authenticate(user: { username: string; password: string }) {
    this.http
      .post('http://localhost:2403/users/login', user)
      .subscribe(data => {
        console.log(data);
      });
  }
}
